import os
from osgeo import ogr, gdal, osr

def find_points_in_tiff(input_directory, output_shapefile, file_indices, name):
    """
    The function is developed with help from AI using ChatGPT. (OpenAI (2024). ChatGPT retrieved 09.04 from https://chatgpt.com/).
	
    Function to map the centre of tiff files i a folder.

    Parameters
    ----------
    input_directory : str
        directory containing the full datasett
    output_shapefile : str
        output shapefile directory
    file_indices : array of int64
        list of indices in the full datasett
    name : str
        name of output shapefile

    Returns
    -------
    Esri Shapefile of mapped points

    """
    driver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(output_shapefile):
        driver.DeleteDataSource(output_shapefile)
    dataSource = driver.CreateDataSource(output_shapefile)

    if dataSource is None:
        print(f"Failed to create the data source at {output_shapefile}. Please check the path and permissions.")
        return

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(25832)

    layer_name = name
    layer = dataSource.CreateLayer(layer_name, srs, ogr.wkbPoint)
    if layer is None:
        print("Failed to create the layer. Check data source compatibility and layer options.")
        return

    layer.CreateField(ogr.FieldDefn("id", ogr.OFTInteger))

    tiff_files = [f for f in os.listdir(input_directory) if f.endswith('.tiff') or f.endswith('.tif')]

    for index in file_indices:
        if 0 <= index < len(tiff_files):
            filename = tiff_files[index]
            ds = gdal.Open(os.path.join(input_directory, filename))
            if ds is None:
                print(f"Failed to open {filename}. Skipping.")
                continue

            transform = ds.GetGeoTransform()
            x_min = transform[0]
            y_max = transform[3]
            x_max = x_min + transform[1] * ds.RasterXSize
            y_min = y_max + transform[5] * ds.RasterYSize

            center_x = (x_min + x_max) / 2
            center_y = (y_min + y_max) / 2

            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetField('id', index)

            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(center_x, center_y)
            feature.SetGeometry(point)
            layer.CreateFeature(feature)
            feature = None

    dataSource = None

